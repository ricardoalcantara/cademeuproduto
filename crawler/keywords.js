var Request = require('request');
var Cheerio = require('cheerio');

//var url = "https://www.google.com.br/search?q=" + "gta";
var url = "https://www.google.com.br/search?q=" + "Jogo Grand Theft Auto: Episode From Liberty City - PS3";

Request({ uri: url, timeout: 10000}, function(err, response, body ){
	var $ = Cheerio.load(body);

	var ret = {};

	$("span.st b").each(function(){
		var regex = /[.]*/;
		var text = $(this).text().replace(regex, "");

		if (text.length > 0){
			if (!ret[text]) ret[text] = 0;
			ret[text]++;
		}
	});

	for(var t in ret){
		console.log(t + " - " + ret[t]);
	}
});