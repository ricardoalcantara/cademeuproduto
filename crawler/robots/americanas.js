 var S = require('string');
var Cheerio = require('cheerio');
var Events = require('events');

var Util = require('../util');
var DateTime = require('../datetime');
var CrawlerQueue = require('../crawlerQueue');
var util = new Util();

var SITEMAP = "sitemap";
var DEPARTAMENT = "departament";
var CATEGORY = "category";

function Americanas(){
  this.store = "americanas";  
  this.baseUrl = "http://www.americanas.com.br/";
  this.crawlerQueue = new CrawlerQueue();
}

Americanas.prototype.__proto__ = Events.EventEmitter.prototype;

Americanas.prototype.sitemap = function(body){
  var that = this;

  var $ = Cheerio.load(body);
  $("loc").each(function(){
    var newUrl = $(this).text();
    if (S(newUrl).endsWith(".xml")){
      that.crawlerQueue.queue(newUrl, SITEMAP);
      //console.log(url);
    } else {
      that.crawlerQueue.queue(newUrl, CATEGORY);
      //console.log(url);
    }
  });
}

Americanas.prototype.departament = function(body){
  var that = this;

  var regexLoja = /\/loja\//;
  var regexCat = /\/linha\//i;
  var regexSubCat = /\/sublinha\//i;

  var $ = Cheerio.load(body);
  $("a").each(function(){

    var newUrl = util.removeQueryString(util.resolveUrl(that.baseUrl, $(this).attr("href")));

    if (regexLoja.test(newUrl)){
      that.crawlerQueue.queue(newUrl, DEPARTAMENT);
      //console.log(newUrl);
    } else if (regexCat.test(newUrl) || regexSubCat.test(newUrl)) {
      that.crawlerQueue.queue(newUrl, CATEGORY);
      //console.log(newUrl);
    }
  });
}

Americanas.prototype.category = function(body, url){
  var that = this;
  var $ = Cheerio.load(body);
console.log(url);
  var regexOffset = /ofertas.offset=(\d+)/i;
  var matchOffset = regexOffset.exec(url);
  var offset = 0;

  if(matchOffset){
    offset = matchOffset[1];
  }

  if(offset == 0){
    var matchTotalPages = /\d+/.exec($("div.rSearch strong:nth-child(3)").first().text());
    var totalPages = 1;
    if( matchTotalPages){
      totalPages = matchTotalPages[0];
    }

    for(i = 1; i < totalPages; i++){i
      var newUrl = util.resolveUrl(url, "?ofertas.offset=" + i*28);
      that.crawlerQueue.queue(newUrl, CATEGORY);
    }
  }

  $("div.prods ul.pList div.hproduct").each(function(){
    var div = $(this);
    var prodUrl = div.find("a").attr("href");
    var thumb = div.find("img").attr("src");
    var data = {
      url: prodUrl,
      store: that.store,
      thumb: thumb
    };

    that.emit("new_link", data);
  });
}

Americanas.prototype.getProduct = function(url, body, callback){
  var that = this;
  var $ = Cheerio.load(body);

  var date = DateTime.now().date;

  var available = $(".unavailProd").length == 0;

  var title = util.chomp(S($("h1.title").text()).trim().s);

  var breadCrumb = [];
  $("ul.breadcrumb li").each(function(){
    breadCrumb.push(util.chomp(S($(this).text()).trim().s));
  });

  var images = [];
  $("div.thumbBox ul.tList li a").each(function(){
    images.push($(this).attr('href'));
  });

  var preco = null;

  if(available){
    var precoDe = util.getCurrency($(".buyBox .regular del.priceOld").text());
    var precoPor = util.getCurrency($(".buyBox .sale span.price").text());
    var precoAvista = util.getCurrency($(".buyBox .priceBankBill").text());
    var precoAvistaDesconto = util.getPercent($(".buyBox .priceBankBill").text());
    var parcelamento = $(".buyBox .parcel").text();

    preco = {
      data: date,
      de: precoDe,
      por: precoPor,
      avista: precoAvista,
      avistaDesconto: precoAvistaDesconto,
      parcelamento: parcelamento
    }
  }
  

  var description = "";

  var product = {
    title: title,
    url: url,
    images: images,
    preco: preco,
    category: breadCrumb,
    description: description,
    available: available,
    store: that.store
  };

  //console.log(product);

  callback(product);
}

Americanas.prototype.request_sucess_callback = function(data){
  var that = this;

  switch(data.type){
    case SITEMAP: {
      that.sitemap(data.body);
      break;
    };
    case DEPARTAMENT: {
      that.departament(data.body);
      break;
    };
    case CATEGORY: {
      that.category(data.body, data.url);   
      break;
    };
  }
}

Americanas.prototype.run = function(){  
  console.log("Americanas started at " + util.getDateTime());

  this.crawlerQueue.on("request_success", this.request_sucess_callback.bind(this));
  this.crawlerQueue.on("drain", function() {
    console.log("Americanas stoped at " + util.getDateTime()); 
  });

  this.crawlerQueue.queue("http://www.americanas.com.br/sitemap_index_acom.xml", SITEMAP);  
  this.crawlerQueue.queue("http://www.americanas.com.br/estatica/todos-os-departamentos/", DEPARTAMENT);  
} 

module.exports = Americanas;
