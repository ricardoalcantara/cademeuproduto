var S = require('string');
var Cheerio = require('cheerio');
var Events = require('events');

var Util = require('../util');
var DateTime = require('../datetime');
var CrawlerQueue = require('../crawlerQueue');
var util = new Util();

var SITEMAP = "sitemap";
var WEBSITEBASE = "websitebase";
var DEPARTAMENT = "departament";
var CATEGORY = "category";

function Walmart(){
  this.store = "walmart";  
  this.baseUrl = "http://www.walmart.com.br/";
  this.crawlerQueue = new CrawlerQueue();
}

Walmart.prototype.__proto__ = Events.EventEmitter.prototype;

Walmart.prototype.sitemap = function(body){
  var that = this;

  var regexDepartament = /\/departamento\//i;
  var regexCategory = /\/categoria\//i;

  var $ = Cheerio.load(body);
  $("loc").each(function(){
    var newUrl = $(this).text();

    if (S(newUrl).endsWith(".xml")){
      that.crawlerQueue.queue(newUrl, SITEMAP);
    } else {

      if (regexDepartament.test(newUrl)){
        that.crawlerQueue.queue(newUrl, DEPARTAMENT);
      } else if (regexCategory.test(newUrl)) {
        that.crawlerQueue.queue(newUrl, CATEGORY);
      }
      //console.log(newUrl);
    }
  });
}

Walmart.prototype.webSiteBase = function(body){
  var that = this;

  var regexDepartament = /\/departamento\//;

  var $ = Cheerio.load(body);
  $(".links-column-department ul li a").each(function(){

    var newUrl = util.resolveUrl(that.baseUrl, $(this).attr("href"));

    if (regexDepartament.test(newUrl)){
      that.crawlerQueue.queue(newUrl, DEPARTAMENT);
      //console.log(newUrl);
    }
  });
}

Walmart.prototype.departament = function(body){
  var that = this;

  var $ = Cheerio.load(body);
  $(".menu-left .menu-title a").each(function(){

    var newUrl = util.cleanQueryString(util.resolveUrl(that.baseUrl, $(this).attr("href")), ['fq', 'PS']);;

    that.crawlerQueue.queue(newUrl, CATEGORY);
    //console.log(newUrl);
  });
}

Walmart.prototype.category = function(body, url){
  var that = this;
  var $ = Cheerio.load(body);
console.log(url);
  var regexPageNumber = /PageNumber=(\d+)/i;
  var matchPageNumber = regexPageNumber.exec(url);
  var pageNumber = 0;

  if(matchPageNumber){
    pageNumber = matchPageNumber[1];
  }

  if(pageNumber == 0){
    var matchTotalPages = /\d+/.exec($("a.shelf-view-more").attr("data-max-pages"));
    var totalPages = 1;
    if( matchTotalPages){
      totalPages = matchTotalPages[0];
    }

    for(i = 2; i <= totalPages; i++){i
      var newUrl = url + "&PageNumber=" + i;

      that.crawlerQueue.queue(newUrl, CATEGORY);
    }
  }

  $("ul.shelf-itens li").each(function(){
    var li = $(this);
    var prodUrl = util.resolveUrl(that.baseUrl, li.find("a").attr("href"));
    var thumb = li.find("img").attr("data-src");
    var data = {
      url: prodUrl,
      store: that.store,
      thumb: thumb
    };
    
    that.emit("new_link", data);
  });
}

Walmart.prototype.getProduct = function(url, body, callback){
  var that = this;
  var $ = Cheerio.load(body);

  var date = DateTime.now().date;

  var available = $(".product-notifyme").length == 0;

  var title = util.chomp(S($(".product-title-header h1").text()).trim().s);

  var breadCrumb = [];
  $("ul.breadcrumb li.breadcrumb-item:not(.home) a span").each(function(){
    breadCrumb.push(util.chomp(S($(this).text()).trim().s));
  });

  var images = [];
  $("div.owl-carousel a.item").each(function(){
    var image = $(this).attr('data-zoom');
    if (image == "") {
      image = $(this).attr('data-normal');
    }

    if (image != "") {
      images.push(image);
    };
  });

  var preco = null;

  if(available){
    var precoDe = util.getCurrency($(".payment-price-old del").text());
    var precoPor = util.getCurrency($(".payment-sell span.payment-price strong").first().text());
    var precoAvista = null;//util.getCurrency($(".buyBox .priceBankBill").text());
    var precoAvistaDesconto = util.getPercent($(".highlights-sale .highlights").text());
    var parcelamento = $(".payment-installment span").text();

    preco = {
      data: date,
      de: precoDe,
      por: precoPor,
      avista: precoAvista,
      avistaDesconto: precoAvistaDesconto,
      parcelamento: parcelamento
    }
  }
  

  var description = "";

  var product = {
    title: title,
    url: url,
    images: images,
    preco: preco,
    category: breadCrumb,
    description: description,
    available: available, 
    store: that.store
  };

  //console.log(product);

  callback(product);
}

Walmart.prototype.request_sucess_callback = function(data){
  var that = this;

  switch(data.type){
    case SITEMAP: {
      that.sitemap(data.body);
      break;
    };
    case WEBSITEBASE: {
      that.webSiteBase(data.body);
      break;
    };
    case DEPARTAMENT: {
      that.departament(data.body);
      break;
    };
    case CATEGORY: {
      that.category(data.body, data.url);   
      break;
    };
  }
}

Walmart.prototype.run = function(){  
  console.log("Walmart started at " + util.getDateTime());

  this.crawlerQueue.on("request_success", this.request_sucess_callback.bind(this));
  this.crawlerQueue.on("drain", function() {
    console.log("Walmart stoped at " + util.getDateTime()); 
  });

  //this.crawlerQueue.queue("http://www.walmart.com.br/departamentos.xml", SITEMAP);  
  this.crawlerQueue.queue(this.baseUrl, WEBSITEBASE);  
} 

module.exports = Walmart;
