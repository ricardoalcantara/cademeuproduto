var S = require('string');
var Cheerio = require('cheerio');
var Events = require('events');

var Util = require('../util');
var DateTime = require('../datetime');
var CrawlerQueue = require('../crawlerQueue');
var util = new Util();

var WEBSITEBASE = "websitebase";
var DEPARTAMENT = "departament";
var CATEGORY = "category";

function CasasBahia(){
  this.store = "casasbahia";  
	this.baseUrl = "http://www.casasbahia.com.br/";
 	this.crawlerQueue = new CrawlerQueue();
}

CasasBahia.prototype.__proto__ = Events.EventEmitter.prototype;

CasasBahia.prototype.webSiteBase = function(body){
  var that = this;

  var regexDepartament = /\/dep\//;

  var $ = Cheerio.load(body);
  $("div.todasCategorias div.todosDept a").each(function(){

    var newUrl = util.resolveUrl(that.baseUrl, $(this).attr("href")|| "");

    if (regexDepartament.test(newUrl)){
      that.crawlerQueue.queue(newUrl, DEPARTAMENT);
    }
  });
}

CasasBahia.prototype.departament = function(body){
  var that = this;

  var regexDepartament = /\/dep\//;
  var regexCat = /\/cat\//i;
  

  var $ = Cheerio.load(body);
  $(".navigation .title2 a").each(function(){

    var newUrl = util.cleanQueryString(util.resolveUrl(that.baseUrl, $(this).attr("href") || ""), ['Filtro']);

    if (regexDepartament.test(newUrl)){
      that.crawlerQueue.queue(newUrl, DEPARTAMENT);
      //console.log(newUrl);
    } else if (regexCat.test(newUrl)) {
      that.crawlerQueue.queue(newUrl, CATEGORY);
      //console.log(newUrl);
    }
  });
}

CasasBahia.prototype.category = function(body, url){
  var that = this;
  var $ = Cheerio.load(body);
console.log(url);
  var regexPaginaAtual = /paginaAtual=(\d+)/i;
  var matchPaginaAtual = regexPaginaAtual.exec(url);
  var paginaAtual = 1;

  if(matchPaginaAtual){
    paginaAtual = matchPaginaAtual[1];
  }

  if(paginaAtual == 1){
    var matchTotalPages = regexPaginaAtual.exec($("div.pagination li.last a").first().attr("href"));
    var totalPages = 1;
    if( matchTotalPages){
      totalPages = matchTotalPages[1];
    }

    for(i = 2; i <= totalPages; i++){i
      var newUrl = url + "&paginaAtual=" + i;

      that.crawlerQueue.queue(newUrl, CATEGORY);
    }
  }

  $("ul.vitrineProdutos li div.hproduct").each(function(){
    var div = $(this);
    var prodUrl = util.removeQueryString(div.find("a").attr("href"));
    var thumb = div.find("img").attr("data-src");
    var data = {
      url: prodUrl,
      store: that.store,
      thumb: thumb
    };

    that.emit("new_link", data);
  });
}

CasasBahia.prototype.getProduct = function(url, body, callback){
  var that = this;
  var $ = Cheerio.load(body);

  var date = DateTime.now().date;

  var available = $(".textoIndisponivel").length == 0;

  var title = S($("div.produtoNome b").first().text()).trim().s;

  var breadCrumb = [];
  $("div.breadcrumb div.wp a:not(.first)").each(function(){
    breadCrumb.push(S($(this).text()).trim().s);
  });

  var images = [];
  $("ul.thumbsImg li a").each(function(){
  	var image = null;
  	var rev = $(this).attr('rev');

  	if (rev != '') {
  		image = rev;
  	}
  	else {
	  	var rel = $(this).attr('href');
	  	if (rel != undefined &&
	  		rel != '') {
	  		image = rel;
	  	}
  	}

    if (image !== null) images.push(image);
  });

  var preco = null;

  if(available){
    var precoDe = util.getCurrency($(".productDetails .from strong").text());
    var precoPor = util.getCurrency($(".productDetails .for strong").text());
    var precoAvista = util.getCurrency($("div.formas .boxFormasD span strong").text());
    var precoAvistaDesconto = null;
    var parcelamento = $(".productDetails .parcel").first().text();

    preco = {
      data: date,
      de: precoDe,
      por: precoPor,
      avista: precoAvista,
      avistaDesconto: precoAvistaDesconto,
      parcelamento: parcelamento
    }
  }
  

  var description = "";

  var product = {
    title: title,
    url: url,
    images: images,
    preco: preco,
    category: breadCrumb,
    description: description,
    available: available,
    store: that.store
  };

  //console.log(product);

  callback(product);
}

CasasBahia.prototype.request_sucess_callback = function(data){
  var that = this;

  switch(data.type){    
    case WEBSITEBASE: {
      that.webSiteBase(data.body);
      break;
    };
    case DEPARTAMENT: {
      that.departament(data.body);
      break;
    };
    case CATEGORY: {
      that.category(data.body, data.url);   
      break;
    };
  }
}

CasasBahia.prototype.run = function(){
	console.log("CasasBahia started at " + util.getDateTime());

	this.crawlerQueue.on("request_success", this.request_sucess_callback.bind(this));
	this.crawlerQueue.on("drain", function() {
    	console.log("CasasBahia stoped at " + util.getDateTime()); 
  	});

	this.crawlerQueue.queue(this.baseUrl, WEBSITEBASE);
}

module.exports = CasasBahia;