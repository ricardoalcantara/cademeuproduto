var Request = require('request');

var RobotsFactory = require('./robotsfactory');

var Util = require('../util');

var robot = new RobotsFactory();
var util = new Util();

var zlib = require('zlib');
//var crawler = robot.factory('casasbahia');
var crawler = robot.factory('americanas');

var urlCasasBahia = [
	'http://www.casasbahia.com.br/Pneu-General-Tire-Altimax-UHP-205-55-R16-4-Unidades-1000032268.html',
	'http://www.casasbahia.com.br/O-Vencedor-Esta-So-Autor-Paulo-Coelho-e-Narrador-Antonio-Fagundes-CD-de-Audiolivro-133524.html?recsource=busca-int&rectype=busca-1376',
	'http://www.casasbahia.com.br/Notebook-Positivo-Unique-S1991-3D-com-Intel-Dual-Core-2GB-250GB-Gravador-de-DVD-Leitor-de-Cartoes-Webcam-LED-14-e-Windows-8-2198047.html?recsource=busca-int&rectype=busca-57',
	'http://www.casasbahia.com.br/Lendo-as-Imagens-do-Cinema-142997.html?recsource=busca-int&rectype=busca-1376',
	'http://www.casasbahia.com.br/Controle-Wireless-Microsoft-Preto-X360-387571.html?recsource=home%E2%80%93336&rectype=Ofertas%20em%20Games%20-int',
	'http://www.casasbahia.com.br/Jogo-Luigi-s-Mansion-Dark-Moon-3DS-1865875.html?recsource=home%E2%80%93336&rectype=Ofertas%20em%20Games%20-int',
	'http://www.casasbahia.com.br/Console-Nintendo-Wii-Black-Core-1-000-Wii-Points-Wii-1719076.html?recsource=home%E2%80%93336&rectype=Ofertas%20em%20Games%20-int'
];

var urlAmericanas = [
	'http://www.americanas.com.br/produto/5468068/aquarela-8-cores-pincel-giz-de-cera-turma-da-monica-18410-molin',
	'http://www.americanas.com.br/produto/113886830/fragmentadora-automatica-100x-swingline'
];

var urlMagazineluiza = [
	'http://www.magazineluiza.com.br/notebook-positivo-sim-2560m-com-intel-dual-core-4gb-500gb-windows-8-led-14/p/0822013/in/note/',
	'http://www.magazineluiza.com.br/xbox-360-250gb-com-kinect-c-1-jogo-kinect-adventures-microsoft-s7g-00027/p/0430703/ga/gaco/',
	'http://www.magazineluiza.com.br/mesa-de-massagem-portatil-profissional-beltex/p/2004147/cp/bsmm/',
	'http://www.magazineluiza.com.br/barbeador-eletrico-c-aquatouch-seco-e-molhado-philips-at751/p/1082079/cp/barb/',
	'http://www.magazineluiza.com.br/escova-modeladora-e-alisadora-rotativa-900w-philco-spin-ion/p/1081942/cp/bsmo/',
	'http://www.magazineluiza.com.br/tablet-multilaser-vibe-nb026-android-4.0-wi-fi-4gb-tela-7-camera-3mp/p/1349893/tb/tbtm/',
	'http://www.magazineluiza.com.br/smartphone-nokia-lumia-520-3g-windows-phone-8-camera-5mp-tela-4-proc.-dual-core-wi-fi-des.-tim/p/0867199/te/tece/',
	'http://www.magazineluiza.com.br/smartphone-dual-chip-samsung-galaxy-s4-mini-duos-3g-android-4.2-cam-8mp-tela-4.3-dual-core-wi-fi/p/0867276/te/tece/',
];

var urlPontofrio = [
	'http://www.pontofrio.com.br/Informatica/Notebook/Notebook-Sony-Vaio-SVE14123CBB-com-Intel-Core-i3-3110M-4GB-500GB-Gravador-de-DVD-Leitor-de-Cartoes-HDMI-Bluetooth-LED-14-e-Windows-8-1865869.html?recsource=busca-int&rectype=busca-57',
	'http://www.pontofrio.com.br/areventilacao/ArCondicionado/Janela/Ar-condicionado-Electrolux-EM10F-Frio-com-Timer-10-000-BTUs-2959.html?recsource=busca-int&rectype=busca-17',
	'http://www.pontofrio.com.br/natal/enfeitesedecoracaodenatal/bonecopapainoel/Boneco-Papai-Noel-com-Esqui-Cromus-1210372-2500918.html',
	'http://www.pontofrio.com.br/natal/enfeitesedecoracaodenatal/velasecasticais/Porta-Vela-Cromus-com-Papai-Noel-1011525-Vermelho-2500909.html',
	'http://www.pontofrio.com.br/eletrodomesticos/FogoeseCooktops/Piso6Bocas/Fogao-Brastemp-6-Bocas-Clean-BF076CR-com-Forno-Smart-Clean-Botoes-Removiveis-e-Acendimento-Automatico-Inox-Bivolt-2256350.html',
	'http://www.pontofrio.com.br/TelefoneseCelulares/CelularesDesbloqueados/Celular-Desbloqueado-LG-Optimus-L5-Preto-E615-com-Dual-Chip-Tela-de-4-Android-4-0-Camera-5MP-3G-Wi-Fi-aGPS-Bluetooth-FM-MP3-e-Fone-Tim-1603236.html',
	'http://www.pontofrio.com.br/Informatica/Notebook/Notebook-Acer-Aspire-E1-571-6422-com-Intel-Core-i5-3230M-2GB-500GB-Gravador-de-DVD-Leitor-de-Cartoes-HDMI-Webcam-LED-15-6-e-Windows-8-2087829.html',
	
]

var urlWalmart = [
	'http://www.walmart.com.br/produto/Bebes/Lancheiras/KaBaby/407475-Lancheira-Temica-Corujinha-Azul-Kababy',
	'http://www.walmart.com.br/produto/Esporte-e-Lazer/Jogos-de-Mesa/Klopf/9798-Mesa-de-Pebolim-1070-Klopf',
	'http://www.walmart.com.br/produto/Informatica/Notebooks-Windows-8/Samsung/363917-Ultrabook-Intel--Core--i5-2537M---NP530U3C-AD3BR',
	'http://www.walmart.com.br/produto/Informatica/Notebooks-Windows-8/Samsung/406982-ATIV-Book-2-Samsung',
	'http://www.walmart.com.br/produto/Beleza-e-Saude/Barbeador-Eletrico/Philips/374815-Aparador-de-Precisao-em-Aco-Philips',
	'http://www.walmart.com.br/produto/Beleza-e-Saude/Massageador/Relax-Medic/242-Esteira-Massageadora-com-Funcao-Aquecimento-Relax-Medic',
	'http://www.walmart.com.br/produto/Informatica/Notebooks-Windows-8/Samsung/363901-Notebook-Intel--Celeron--Dual-Core-B820---NP300E4C',
	'http://www.walmart.com.br/produto/Informatica/Notebooks-Windows-8/Positivo/405648-NOTEBOOK-PREMIUM-S2940',
]

//var urls = urlAmericanas.concat(urlCasasBahia);
var urls = urlWalmart;
//var url = 	'http://www.magazineluiza.com.br/notebook-positivo-sim-2560m-com-intel-dual-core-4gb-500gb-windows-8-led-14/p/0822013/in/note/';
var url = 'http://www.pontofrio.com.br/Games/PC/JogosparaPC/Jogo-Fifa-Manager-13-PC-1766023.html'; 
//urls = [url];
/*for (var i = 0; i < 10; i++) {
	console.log(robot.factory("americanas"));
};*/

//process.exit();

var headers = {
        "User-Agent": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
        "Accept": "text/html, application/xhtml+xml, */*",
        "Accept-Language": "pt-BR",
        //"Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive"
    };
/*
Request({ uri: 'http://www.cademeuproduto.com.br', timeout: 10000, headers: headers, encoding: null}, function(err, response, body){
	if (err) { console.log(err); };
	console.log(response.statusCode);

	console.log(response.headers[ 'content-encoding' ]);
	console.log(body);

	zlib.gunzip(body, function (err, b){
		console.log(b.toString());
	});
});
*/
for (var i = 0; i < urls.length; i++) {	
	(function(url){
		Request({ uri: url, timeout: 10000, headers: headers}, function(err, response, body){
			if (err) { console.log(err); };
			console.log(response.statusCode);

			var me = robot.whoAmI(url);
			robot.factory(me).getProduct(url, body, function(product){
				console.log(product);
			});			
		});
	}(urls[i]));
}