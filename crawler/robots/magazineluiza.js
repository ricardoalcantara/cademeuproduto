var S = require('string');
var Cheerio = require('cheerio');
var Events = require('events');

var Util = require('../util');
var DateTime = require('../datetime');
var CrawlerQueue = require('../crawlerQueue');
var util = new Util();

var WEBSITEBASE = "websitebase";
var DEPARTAMENT = "departament";
var CATEGORY = "category";

function Magazineluiza(){
  this.store = "magazineluiza";
  this.baseUrl = "http://www.magazineluiza.com.br/";
  this.crawlerQueue = new CrawlerQueue();
}

Magazineluiza.prototype.__proto__ = Events.EventEmitter.prototype;

Magazineluiza.prototype.webSiteBase = function(body){
  var that = this;

  var $ = Cheerio.load(body);
  $("div.left-all-menus div.middle-all-menus div.box-all-menus a").each(function(){

    var newUrl = util.resolveUrl(that.baseUrl, $(this).attr("href"));

    that.crawlerQueue.queue(newUrl, DEPARTAMENT);
  });
}

Magazineluiza.prototype.departament = function(body){
  var that = this;

  var $ = Cheerio.load(body);
  $("li.line-product a").each(function(){

    var newUrl = util.resolveUrl(that.baseUrl, $(this).attr("href"));
    
    that.crawlerQueue.queue(newUrl, CATEGORY);
  });
}

Magazineluiza.prototype.category = function(body, url){
  var that = this;
  var $ = Cheerio.load(body);
console.log(url);
  var regexPaginaAtual = /\/(\d+)\//i;
  var matchPaginaAtual = regexPaginaAtual.exec(url);
  var paginaAtual = 0;

  if(matchPaginaAtual){
    paginaAtual = matchPaginaAtual[1];
  }

  if(paginaAtual == 0){
    var matchTotalPages = regexPaginaAtual.exec($("a.last-page").first().attr("href"));
    var totalPages = 1;
    if( matchTotalPages){
      totalPages = matchTotalPages[1];
    }

    for(i = 2; i <= totalPages; i++){i
      var newUrl = url + i + "/";

      that.crawlerQueue.queue(newUrl, CATEGORY);
    }
  }

  $("li.product").each(function(){
    var div = $(this);

    var prodUrl = util.resolveUrl(that.baseUrl, div.find("a").attr("href"));
    var thumb = div.find("img").attr("data-original");
    var data = {
      url: prodUrl,
      store: that.store,
      thumb: thumb
    };

    that.emit("new_link", data);
  });
}

Magazineluiza.prototype.getProduct = function(url, body, callback){
  var that = this;
  var $ = Cheerio.load(body);

  var date = DateTime.now().date;

  var available = $(".container-warning-out").length == 0;

  var title = $('h1').text();

  var breadCrumb = [];
  $("ul.breadcrumb-list-product li").each(function(){
    var li = $(this);

    if (li.attr("typeof") == "v:Breadcrumb") {
      breadCrumb.push(S(li.text()).trim().s);
    }
  });

  var images = [];
  $("div.rs-carousel .rs-carousel-runner .rs-carousel-item a").each(function(){
    var regexLarge = /largeimage:[^']*'([^']+)'/;
    var regexSmall = /smallimage:[^']*'([^']+)'/;
    var image = null;

    var rel = $(this).attr('rel');
  
    if (rel !== undefined) {
      var str = JSON.stringify(rel);
      var matchLargeImage = regexLarge.exec(str);

      if (matchLargeImage) {
        image = matchLargeImage[1];
      } 
      else {
        var matchSmallImage = regexSmall.exec(str);

        if (matchSmallImage) {
          image = matchSmallImage[1];
        }
      }
    }

    if (image !== null) images.push(image);
  });

  var preco = null;

  if(available){
    var precoDe = util.getCurrency($(".container-price-product .top-price").text());
    var precoPor = util.getCurrency($(".container-price-product .right-price").text());
    var precoAvista = util.getCurrency($(".container-extra-product .txt-rates-product strong").text());
    var precoAvistaDesconto = util.getPercent($(".container-extra-product .txt-rules-produtc").text());
    var parcelamento = $(".container-extra-product .txt-rates-product span").text();

    preco = {
      data: date,
      de: precoDe,
      por: precoPor,
      avista: precoAvista,
      avistaDesconto: precoAvistaDesconto,
      parcelamento: parcelamento
    }
  }
  

  var description = "";

  var product = {
    title: title,
    url: url,
    images: images,
    preco: preco,
    category: breadCrumb,
    description: description,
    available: available,
    store: that.store
  };

  //console.log(product);

  callback(product);
}

Magazineluiza.prototype.request_sucess_callback = function(data){
  var that = this;

  switch(data.type){
    case WEBSITEBASE: {
      that.webSiteBase(data.body);
      break;
    };
    case DEPARTAMENT: {
      that.departament(data.body);
      break;
    };
    case CATEGORY: {
      that.category(data.body, data.url);   
      break;
    };
  }
}

Magazineluiza.prototype.run = function(){  
  console.log("Magazineluiza started at " + util.getDateTime());

  this.crawlerQueue.on("request_success", this.request_sucess_callback.bind(this));
  this.crawlerQueue.on("drain", function() {
    console.log("Magazineluiza stoped at " + util.getDateTime()); 
  });

  this.crawlerQueue.queue(this.baseUrl, WEBSITEBASE);  
} 

module.exports = Magazineluiza;