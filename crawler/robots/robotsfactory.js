
var Americanas = require('./americanas');
var CasasBahia = require('./casasbahia');
var PontoFrio = require('./pontofrio');
var Magazineluiza = require('./magazineluiza');
var Walmart = require('./walmart');
var Submarino = require('./submarino');


function robotFactory() {
	this.cacheObj = {};
}

robotFactory.prototype.factory = function (robot){
	var that = this;
	var objRobot;
	switch(robot){
		case "americanas":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new Americanas();
			}

			objRobot = that.cacheObj[robot];
			break;
		}
		case "casasbahia":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new CasasBahia();
			}
			
			objRobot = that.cacheObj[robot];
			break;
		}
		case "pontofrio":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new PontoFrio();
			}
			
			objRobot = that.cacheObj[robot];
			break;
		}
		case "magazineluiza":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new Magazineluiza();
			}
			
			objRobot = that.cacheObj[robot];
			break;
		}
		case "walmart":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new Walmart();
			}
			
			objRobot = that.cacheObj[robot];
			break;
		}
		case "submarino":{
			if (that.cacheObj[robot] == undefined) {
				that.cacheObj[robot] = new Submarino();
			}
			
			objRobot = that.cacheObj[robot];
			break;
		}				
		default: {
			objRobot = null;
		}
	}

	return objRobot;
}

robotFactory.prototype.whoAmI = function (url){
	//(\bhttp://)?(\bwww.)?([a-zA-Z0-9]+)($|(.com|.br))
	var reg = /(\bhttp:\/\/)?(\bwww.)?([a-zA-Z0-9]+)($|(.com|.br))/;
	var match = reg.exec(url);

	var someone = "unknown";

	if (match) {
		someone = match[3];
	};

	return someone;
}

module.exports = robotFactory;