var Async = require('async');
var Redis = require('redis');
var MongoClient = require('mongodb').MongoClient;

var Util = require('./util');
var DateTime = require('./datetime');

var config = require('./config.json');

var util = new Util();

var LIST_NEW = "l:new";
var LIST_UPDATE = "l:update";

function Manager(){
  this.redis = Redis.createClient(config.redis.port, config.redis.host);
  this.mongodb;
  this.collection;
}

Manager.prototype.check_new_links = function(that){
  that.check(LIST_NEW, function(){
    console.log("check new links at " + DateTime.now().toString());

    that.collection.find( {last_update: null} ).toArray(function(err, docs){
      if(err) console.log(err);

      console.log("Total new links found: " + docs.length);

      that.enqueue(docs, LIST_NEW);

      setTimeout(that.check_new_links, 30000, that);
    });
  });
}

Manager.prototype.refresh_links = function(that){
  that.check(LIST_UPDATE, function(){
    console.log("refresh links at " + DateTime.now().toString());

    that.collection.find( {last_update: { $lte : DateTime.now().addHours(-6) }}).toArray(function(err, docs){
      if(err) console.log(err);

      console.log("Total refresh links found: " + docs.length);

      that.enqueue(docs, LIST_UPDATE);

      setTimeout(that.refresh_links, 30000, that);
    });

  });
}

Manager.prototype.check = function(list, callback){
  var that = this;

  console.log("Checking: " + list);
  that.redis.llen(list, function(err, length){
    if (length == 0) {
      callback();
    }
    else {

      switch( list ){
      case LIST_NEW:{
        setTimeout(that.check_new_links, 30000, that);

        break;
      }
      case LIST_UPDATE:{
        setTimeout(that.refresh_links, 30000, that);

        break;
      }
    }
      console.log("redis full: " + list);
    }
  });
}

Manager.prototype.enqueue = function(docs, list){
  var that = this;

  console.log("enqueuing");
  for(i = 0; i < docs.length; i++){
    (function(doc){
      that.redis.rpush(list, JSON.stringify(doc), function(err){
        if (err) {console.log(err)};
      });
    }(docs[i]));
  }
}

Manager.prototype.run = function(){
  var that = this;

  console.log("Started at " + DateTime.now().toString());

  MongoClient.connect(config.mongodb.connectionString + config.mongodb.database.product.db, function(err, db){
    that.mongodb = db;
    that.collection = that.mongodb.collection(config.mongodb.database.product.collection);

    process.nextTick(function() { that.check_new_links(that) });
    process.nextTick(function() { that.refresh_links(that) });
  });
}

module.exports = Manager;