var URI = require('url');
var crypto = require('crypto');

function Util() {}

Util.prototype.removeQueryString = function(url) {
  var objUrl = URI.parse(url);

  return URI.resolve(objUrl.href, objUrl.pathname);
}

Util.prototype.cleanQueryString = function(url, items){
  var objUrl = URI.parse(url);
  var newUrl = URI.resolve(objUrl.href, objUrl.pathname);
  var newQuery = '';

  if (objUrl.query == null) {
    return newUrl;  
  };

  var queryItems = (objUrl.query || '').split('&');

  for(var i = 0;i < queryItems.length; i++){
    var queryItemValue = queryItems[i].split('=');

    for (var j = 0; j < items.length; j++) {
      if(queryItemValue[0] == items[j]){
        newQuery += '&' + queryItemValue[0] + '=' + queryItemValue[1];
        break;
      }
    }
  }
  
  return (newUrl + '?' + newQuery.substring(1));  
}

Util.prototype.resolveUrl = function(base, path) {
  return URI.resolve(base, path)
}

Util.prototype.chomp = function(str){
  if (str.indexOf('\n') == -1) {
    return str;
  };
  
  return str.substr(0, str.indexOf('\n'))
}

Util.prototype.getCurrency = function(value) {
  var regexCurrency = /([.,\d]+)/;
  var match = regexCurrency.exec(value);
  var currency = 0;
  if(match){
    currency = match[1];
  }

  return currency
}

Util.prototype.getPercent = function(value) {
  var regexPercent = /(\d+%)/;
  var match = regexPercent.exec(value);
  var percent = null;
  if(match){
    percent = match[1];
  }

  return percent;
}

Util.prototype.getDate = function() {
    var d = new Date();
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}

Util.prototype.getDateTime = function() {
    var d = new Date();

    return d.toString() ;
}

Util.prototype.getHash = function(value) {
  if(value === undefined || value == "") console.log(value)    
  return crypto.createHash('md5').update(value).digest("hex");
}

Util.prototype.uniqueInArray = function(origArr) {
    var newArr = [];
    var origLen = origArr.length;
    var found;
       
    for ( x = 0; x < origLen; x++ ) {
        found = undefined;
        for ( y = 0; y < newArr.length; y++ ) {
            if ( origArr[x] === newArr[y] ) { 
              found = true;
              break;
            }
        }
        if ( !found) newArr.push( origArr[x] );    
    }
   return newArr;
}

module.exports = Util;
