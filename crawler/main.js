var argv = require('optimist')
            .boolean(["details","manager"])
            .argv;

var Crawler = require('./crawler');
var Manager = require('./manager');
var Details = require('./details');

if (argv.crawler){
  var crawler = new Crawler();
  crawler.run(argv.crawler);
}
else if (argv.details){
  var details = new Details();
  details.run();
}
else if (argv.manager){
  var manager = new Manager();
  manager.run();
}
