var Async = require('async');
var Request = require('request');
var Events = require('events');
var Async = require('async');

function getHeaders(){
  return headers = {
        "User-Agent": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
        "Accept": "text/html, application/xhtml+xml, */*",
        "Accept-Language": "pt-BR",
        //"Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive"
    };
}

function CrawlerQueue(){
	this.cache = {};

	this.q = Async.queue(function(task, callback){
		Request({ uri: task.url, timeout: 60000, headers : getHeaders()}, function (err, response, body){
      		if (err) {
            console.log("ERROR: " + task.url);
            console.log(err);
          }
      		var that = task.that;

      		that.emit("request_success", {
      			response : response,
      			body : body,
      			url : task.url,
      			type : task.type
      		});

	    	callback();
	    });
	}, 10);

	this.q.drain = function() {
		this.emit("drain");
	}.bind(this);
}

CrawlerQueue.prototype.__proto__ = Events.EventEmitter.prototype;

CrawlerQueue.prototype.queue = function(url, type, baseUrl){
  if (this.cache[url])return;
  else this.cache[url] = true;

  this.q.push({url : url, type: type, that: this}, function(err){
    if (err) console.log(err);
  });
}

module.exports = CrawlerQueue;