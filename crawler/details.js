var Async = require('async');
var Redis = require('redis');
var Request = require('request');
var MongoClient = require('mongodb').MongoClient;
var ES = require('es');
var Sleep = require('sleep');

var Util = require('./util');
var DateTime = require('./datetime');

var config = require('./config.json');

var RobotsFactory = require('./robots/robotsfactory');

var util = new Util();

var LIST_NEW = "l:new";
var LIST_UPDATE = "l:update";

function getHeaders(){
  return headers = {
        "User-Agent": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
        "Accept": "text/html, application/xhtml+xml, */*",
        "Accept-Language": "pt-BR",
        //"Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive"
    };
}

function Details(){
  this.redis = Redis.createClient(config.redis.port, config.redis.host);
  this.robot = new RobotsFactory();
  this.es = ES.createClient(config.es.config);
  this.mongodb;
  this.collection;
  this.mode;
}

Details.prototype.getNextList = function(){
  return LIST_NEW;
}


Details.prototype.checkListTodo = function(callback){
  var that = this;
  that.redis.rpop(that.getNextList(), function(err, doc){
    if(doc == null){
      console.log("the queue is empty");
      Sleep.sleep(2);
      callback();
    }
    else {
      var json = JSON.parse(doc);

      if (json === null){
        console.log("error: " + doc);
        callback();
        return;
      }

      that.request(json, callback);
    }
  });
}

Details.prototype.request = function(json, callback){
  var that = this;
  Request({uri: json.url, timeout: 10000, headers : getHeaders()}, function(err, response, body){
    if (err) {
      console.log("ERROR: " + json.url);
      console.log(err);
    }

    //refatorá para fazer async =DDD
    that.robot.factory(that.robot.whoAmI(json.url)).getProduct(json.url, body, function(product){

      if(json.price == null){
        json.price = [];
      }
    
      //tem q comparar ainda né mané!!!!
      if (product.preco != null) {
        json.price.push(product.preco);
      };

      var p = {
        title : product.title,
        price : json.price,
        last_update : DateTime.now().date,
        thumb : json.thumb,
        images : product.images,
        category : product.category,
        description : product.description,
        available : product.available,
        store : product.store,
        url : json.url
      }

      that.updateDB(json, p, callback);    
    });
  });
}

Details.prototype.updateDB = function(json, p, callback){
  var that = this;
  that.collection.update({_id: json._id }, {$set: p }, {safe:true}, function(err){
    if (err) console.log(err);

    var c = config.es.index.product;
    c._id = json._id;

    that.es.index(c, p, function(err, data){
      if(err) console.log(err);
      
      console.log("Done: " + json.url);

      setImmediate(callback);
    });
  });
}

Details.prototype.run = function(){
  console.log("started at " + util.getDateTime());
  var that = this;
  MongoClient.connect(config.mongodb.connectionString + config.mongodb.database.product.db, function(err, db){
    that.mongodb = db;
    that.collection = that.mongodb.collection(config.mongodb.database.product.collection);
    
    for(var i = 0; i < 10; i++){
      Async.forever(that.checkListTodo.bind(that)); 
    }
  });
}

module.exports = Details;
