var MongoClient = require('mongodb').MongoClient;

var config = require('./config.json');
var Util = require('./util');

var RobotsFactory = require("./robots/robotsfactory");

var util = new Util();

function Crawler(){
  this.mongodb;
  this.collection;
  this.robotsFactory = new RobotsFactory();
}

Crawler.prototype.new_link_callback = function(data){
  var that = this;

  var id = util.getHash(data.url);
  that.collection.find( {_id: id }, {_id:true, thumb:true}).nextObject(function(err, doc){
    if (err) console.log(err);

    if(!doc){
    var ndoc = {
        _id: id,
        url: data.url,
        thumb: data.thumb,
        last_update: null,
        store: data.store
      };

      that.collection.insert(ndoc, {safe:true}, function(err, docs){
        if (err) console.log(err);
      });
    } else if(doc.thumb == null){
      that.collection.update({_id:doc._id}, {$set: { thumb : data.thumb }},{safe:true}, function(err, docs){
        if (err) console.log(err);
      });
    }
  });
}

Crawler.prototype.run = function(store){
  var that = this;

  var robot = this.robotsFactory.factory(store);


  robot.on("new_link", that.new_link_callback.bind(this));

  MongoClient.connect(config.mongodb.connectionString + config.mongodb.database.product.db, function(err, db){
    that.mongodb = db;
    that.collection = that.mongodb.collection(config.mongodb.database.product.collection);

    robot.run();

  });
}

module.exports = Crawler;
