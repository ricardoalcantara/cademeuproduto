var second = 1000;;
var minute = 60 * second;
var hour = 60 * minute;
var day = 24 * hour;
//var month = 0;
//var year =  0;

function DateTime(){
  this.date;

  switch (arguments.length){
   case 1:{
      this.date = new Date(arguments[0]);
      break;
    }
    case 3:{
      this.date = new Date(arguments[0],arguments[1],arguments[2]);
      break;
    }
    case 6:{
      this.date = new Date(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);
      break;
    } 
    default:{
      this.date = new Date();
      break;
    }
  }
}

DateTime.prototype.toString = function(){
  return this.date.toString();
}

DateTime.prototype.addSeconds = function(seconds){
  var d = new Date(this.date.getTime() + (seconds * second));
  this.date = d;
  return this.date;
}

DateTime.prototype.addMinutes = function(minutes){
  var d = new Date(this.date.getTime() + (minutes * minute));
  this.date = d;
  return this.date;
}

DateTime.prototype.addHours = function(hours){
  var d = new Date(this.date.getTime() + (hours * hour));
  this.date = d;
  return this.date;
}

DateTime.prototype.addDays = function(days){
  var d = new Date(this.date.getTime() + (days * day));
  this.date = d;
  return this.date;
}

DateTime.prototype.addMonths = function(months){
  this.date.setMonth(this.date.getMonth() + months);
  return this.date;
}

DateTime.prototype.addYears = function(years){
  this.date.setFullYear(this.date.getFullYear() + years);;
  return this.date;
}

DateTime.now = function(){
  return new DateTime();
}

module.exports = DateTime;
