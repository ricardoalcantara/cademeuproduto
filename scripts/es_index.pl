#!/urs/bin/env perl

use common::sense;
use JSON;
use File::Find::Rule;
use DDP;
use Encode qw(decode encode);
use REST::Client;
use URI;

my $client = REST::Client->new();

my @files =
File::Find::Rule->file->name('*.js')->in("/home/ricardo/products");

for my $file (@files){
  my $text =  do { local (@ARGV, $/) = $file; <> };
  $text =~ s/}\n{/,/g;
  my $json = from_json($text);
say $file;
  foreach ( keys $json){

    $_ =~ /http:\/\/([^\/]+)/;
    my $data = {
                  title => $json->{$_}->{title},
                  brand => $json->{$_}->{brand},
                  seller => $1,
                  name => $json->{$_}->{product},
                  thumb => $json->{$_}->{thumb},
                  image => $json->{$_}->{images}->{central},
                  description => $json->{$_}->{description},
                  designer => $json->{$_}->{thumbnail},
                  url => $_
                };

    $client->POST("http://localhost:9200/t/cosmeticos", to_json($data));
  }
}

say "DONE!";

