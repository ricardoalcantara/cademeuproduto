use common::sense;
use Elasticsearch;
use JSON;
use File::Find::Rule;
use DDP;
use Encode qw(decode encode);

my $search = shift @ARGV; 
my $es = Elasticsearch->new();

my $result = $es->search( index => 't',
                type => 'cosmeticos',
                from => 0,
                size => 200,
                body => {
                  query => {
                    text => {
                      name => {
                        query => $search,
                        operator => "AND"
                      }
                    }
                  }
                });

foreach(@{$result->{hits}->{hits}}){
  say $_->{_source}->{name};
}

say "$result->{hits}->{total}";
