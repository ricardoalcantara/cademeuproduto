#!/urs/bin/env perl

use common::sense;
use JSON;
use File::Find::Rule;
use DDP;
use Encode qw(decode encode);
use REST::Client;
use URI;

my $client = REST::Client->new();

my @files =
File::Find::Rule->file->name('*.json')->in("/home/ricardo/MLB1246");

for my $file (@files){

  my $json = from_json( do { local (@ARGV, $/) = $file; <> });
  for my $result ( @{$json->{results}}){

    if ($result->{buying_mode} eq "buy_it_now") {

     $result->{permalink} =~ /http:\/\/([^\/]+)/;

     my $data = {
                    title => $result->{title},
                    brand => '',
                    #seller => "MLB$result->{seller}->{id}",
                    seller => "www.mercadolivre.com.br",
                    price => $result->{price},
                    name => $result->{subtitle},
                    thumb => $result->{thumbnail},
                    image => $result->{thumbnail},
                    description => '', #$result->{description},
                    designer => '',
                    url => $result->{permalink}
                  };

      $client->POST("http://localhost:9200/t/cosmeticos_meli", to_json($data));
    }
  }
}

say "DONE!";

