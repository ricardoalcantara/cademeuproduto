#!/usr/bin/env perl
package Crawler::Price::Walmart;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.descricao-preco', price => scraper {
      process '.valor-de', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.valor-por', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.preco-a-vista', avista => ['text', sub{/([.,\d]+)/;$1}];
      process '.valor-dividido', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Walmart->new(
  url =>
'http://www.walmart.com.br/produto/Esporte-e-Lazer/Estacoes-e-Academias-de-Ginastica/Titanium-Fitness/357008-Estacao-de-Musculacao-Titanium-Residencial' 
)->get_price; 
p$test;
