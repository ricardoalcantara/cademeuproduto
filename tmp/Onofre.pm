#!/usr/bin/env perl
package Crawler::Price::Onofre;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.preco_prod', price => scraper {
      process '.preco_prod_cinza', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.preco_por', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.parcela', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Onofre->new(
  url => 
'http://www.onofre.com.br/bio-oil-oleo-para-o-cuidado-da-pele-60ml/56875/05'
)->get_price; 
p$test;
