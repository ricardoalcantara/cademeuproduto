#!/usr/bin/env perl
package Crawler::Price::Extra;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '#ctl00_Conteudo_869', price => scraper {
      process '.productDetails .from', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.productDetails .for', por => ['text', sub{/([.,\d]+)/;$1}];
      #process '.priceBankBill', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.productDetails .parcel', parcelamento => 'text';
      process '.formas span', avista => ['text', sub{/([.,\d]+)/;$1}]
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Extra->new(
  url =>
'http://www.extra.com.br/TelefoneseCelulares/Smartphones/Celular-Desbloqueado-LG-Nexus-4-Branco-com-Tela-4-7”-Processador-de-1-5-GHz-Android-4-2-Camera-8MP-3G-Wi-Fi-Bluetooth-e-NFC-2224406.html?csParam=%7B%22feature%22:%22ultimatebuy%22,%22source%22:%22product%20details%22,%22recType%22:%22ultimatebuy%22%7D&recsource=chaordic&rectype=product-details_ultimatebuy'
)->get_price; 
p$test;
