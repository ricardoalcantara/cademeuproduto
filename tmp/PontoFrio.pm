#!/usr/bin/env perl
package Crawler::Price::Americanas;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '#ctl00_Conteudo_3015', price => scraper {
      process '.productDetails .from', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.productDetails .for', por => ['text', sub{/([.,\d]+)/;$1}];
      #process '.priceBankBill', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.productDetails .parcel', parcelamento => 'text';
      process '.formas span', avista => ['text', sub{/([.,\d]+)/;$1}]
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Americanas->new(
  url => 
'http://www.pontofrio.com.br/eletrodomesticos/FornodeMicroondas/Forno-de-Microondas-Electrolux-Faca-Facil-MEF28-18-L-25169.html?csParam={%22feature%22:%22viewpersonalized%22,%22source%22:%22home%22,%22recType%22:%22viewpersonalized%22}&recsource=chaordic&rectype=home_viewpersonalized'
)->get_price; 
p$test;
