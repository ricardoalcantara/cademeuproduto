#!/usr/bin/env perl
package Crawler::Price::CasaeVideo;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.price', price => scraper {
      process 'del.list-price b', de => 'text';
      process 'em.sale-price b', por => 'text';
      process 'small.savings b', avista => 'text';
      process 'small.savings', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.condition', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::CorpoIdealSuplementos->new(
  url =>
'http://www.casaevideo.com.br/Camera-Digital/1-10-232-163220/Camera-Digital-16MP-Nikon-L810-com-Zoom-Optico-26x-LCD-de-3"-Grava-Video-em-HD-Foto-3D-Panoramica-e-Cartao-de-Memoria-SD-4GB-Preta.html'
)->get_price; 
p$test;
