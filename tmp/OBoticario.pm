#!/usr/bin/env perl
package Crawler::Price::OBoticario;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.preco', price => scraper {
      process '.valor-de', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.valor-por', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.valor-dividido', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::OBoticario->new(
  url => 
'http://www.boticario.com.br/Zaad-Eau-de-Parfum-95ml-09685/p'
)->get_price; 
p$test;
