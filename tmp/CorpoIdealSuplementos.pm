#!/usr/bin/env perl
package Crawler::Price::CorpoIdealSuplementos;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.price', price => scraper {
      process 'del.list-price b', de => 'text';
      process 'em.sale-price b', por => 'text';
      process 'small.savings b', avista => 'text';
      process 'small.savings', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.condition', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::CorpoIdealSuplementos->new(
  url =>
'http://www.corpoidealsuplementos.com.br/oxyhers-90-liquidfast-caps-arnold-nutrition-p3516/'
)->get_price; 
p$test;
