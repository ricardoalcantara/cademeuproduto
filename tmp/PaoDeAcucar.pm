#!/usr/bin/env perl
package Crawler::Price::PaoDeAcucar;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );
has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.box-price', price => scraper {
      process '.price-off', avista => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::PaoDeAcucar->new(
  url =>
'http://www.paodeacucar.com.br/produto/99219/biscoito-qualita-de-polvilho-queijo-pacote-100g'
)->get_price; 
p$test;
