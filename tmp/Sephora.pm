#!/usr/bin/env perl
package Crawler::Price::Sephora;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.precoSKU', price => scraper {
      process '.produtoPrecoVendaDe', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.produtoPrecoVendaPor', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.produtoParcela', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Sephora->new(
  url => 
'http://www.sephora.com.br/site/produto.asp?id=4404'
)->get_price; 
p$test;
