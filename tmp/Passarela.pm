#!/usr/bin/env perl
package Crawler::Price::Passarela;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '#valorDiv', price => scraper {
      process '.parcela:first-child', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.preco', por => ['text', sub{/([.,\d]+)/;$1}];
      process '//p[@class="parcela"][last()]', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Passarela->new(
  url => 
'http://www.passarela.com.br/feminino/produto/6030329116/Sapato-Lara-6554101----Vermelho/'
#'http://www.passarela.com.br/outlet/produto/6050082615/Sapato-Slipper-Via-Marte-13-2003---Verde/'
)->get_price; 
p$test;
