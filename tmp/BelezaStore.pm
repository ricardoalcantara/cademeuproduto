#!/usr/bin/env perl
package Crawler::Price::BelezaStore;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.price', price => scraper {
      process '.list-price .list', de => 'text';
      process '.sale-price .sale', por => 'text';
      process '.savings .sale', avista => 'text';
      process '.savings', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.condition', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::BelezaStore->new(
  url => 'http://www.belezastore.com.br/aussie-hair-insurance-leave-condicionador-para-cabelos-fracos-e-ressecados-150ml-p1665'
)->get_price; 
p$test;
