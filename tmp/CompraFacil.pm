#!/usr/bin/env perl
package Crawler::Price::CompraFacil;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.produto-compra-info', price => scraper {
      process '.produto-de strong', de => 'text';
      process '.produto-por strong', por => 'text';
      process '.parcela strong', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::CompraFacil->new(
  url =>
'http://www.comprafacil.com.br/comprafacil/Produto.jsf?VP=59,241832,241930,241937,194110&VPP=MODA+RELOGIOS+FEMININO+RELOGIO+FEMININO+SECULUS+ANALOGICO+SIMPLES+CAIXA+E+PULSEIRA+EM+TITANIUM+PRATA+20089L0SPNT1'
)->get_price; 
p$test;
