#!/usr/bin/env perl
package Crawler::Price::Panvel;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.maincontent_inner', price => scraper {
      process '.etiqueta_preconormal .depor,.etiqueta_precopromocional .depor', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.etiqueta_preconormal,.etiqueta_precopromocional', por => ['text', sub{/([.,\d]+\z)/;$1}];
      process '.parcelamento li:last-child', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Panvel->new(
  #url => 'http://www.panvel.com/panvel/visualizarProduto.do?codigoItem=535520'
  url => 'http://www.panvel.com/panvel/visualizarProduto.do?codigoItem=988180'
)->get_price; 
p$test;
