#!/usr/bin/env perl
package Crawler::Price::MagazineLuiza;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.container-price-product', price => scraper {
      process '.top-price', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.right-price', por => ['text', sub{/([.,\d]+)/;$1}];
    };
    process 'div.container-extra-product', price2 => scraper {
      process '.txt-rates-product span', parcelamento => 'text';
      process '.txt-rates-product strong', avista => ['text', sub{/([.,\d]+)/;$1}];
      process 'span.txt-rules-produtc', desconto => ['text', sub{/(\d+%)/;$1}];
    }
}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::MagazineLuiza->new(
  url =>
'http://www.magazineluiza.com.br/smartphone-3g-sony-xperia-l-android-4.1-camera-8mp-tela-4.3-proc.-dual-core-wi-fi-a-gps/p/0867242/te/tece/'
)->get_price; 
p$test;
