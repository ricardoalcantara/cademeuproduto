#!/usr/bin/env perl
package Crawler::Price::Americanas;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
      process '//body//p', 'descricao[]' => 'text';
}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Americanas->new(
  url =>
'http://www.aazperfumes.com.br/listaprodutos.asp?idloja=434&idproduto=20085&q=CAROLINA+HERRERA+FOR+MEN+EAU+DE+TOILETTE+MASCULINO'
)->get_price; 
p$test;
