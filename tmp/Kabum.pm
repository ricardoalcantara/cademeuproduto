#!/usr/bin/env perl
package Crawler::Price::Kabum;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '#desc_precos', price => scraper {
      process 'tr:nth-child(7) font', de => ['text', sub{/([.,\d]+)/;$1}];
      process 'tr:nth-child(7) sub font', por => ['text', sub{/([.,\d]+)/;$1}];
      process 'tr:nth-child(9) font font', avista => ['text', sub{/([.,\d]+)/;$1}];
      process 'tr:nth-child(9) font u', desconto => ['text', sub{/(\d+%)/;$1}];
      process 'tr:nth-child(8)', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Kabum->new(
  url => 'http://www.kabum.com.br/produto/32231/processador-intel-ivy-bridge-core-i3-3240-3'
)->get_price; 
p$test;
