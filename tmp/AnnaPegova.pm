#!/usr/bin/env perl
package Crawler::Price::AnnaPegova;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );
has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process 'div.preco_detalhe_produto', price => scraper {
      process 'span.preco_produto_detalhe span', avista => 'text';
      process '#ctl00_ContentSite_spnParcels', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::AnnaPegova->new(
  url =>
'http://www.annapegova.com.br/mascara-volume-et-longueur--crayon-retractable-pour-les-yeux,product,1531,dept,111.aspx'
)->get_price; 
p$test;
