#!/usr/bin/env perl
package Crawler::Price::Americanas;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '#orcamento-produto', price => scraper {
      process '.pagamento-old', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.preco-normal', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.desconto-destaque', avista => ['text', sub{/([.,\d]+)/;$1}];
      process '.deconto', boleto => ['text', sub{/([.,\d]+)/;$1}];
      process '.preco-vezes', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Americanas->new(
  url =>
#'http://www.efacil.com.br/DetalheProduto.aspx?CodMer=3300466' 
'http://www.efacil.com.br/DetalheProduto.aspx?CodMer=3300679'
)->get_price; 
p$test;
