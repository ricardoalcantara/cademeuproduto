#!/usr/bin/env perl
package Crawler::Price::Americanas;
use use qw[ common::sense Moo Web::Scraper URI ];

has 'url'  => (is => 'rw', );

has '_dom' => (is => 'ro', default => sub{
  return scraper {
    process '.buyBox', price => scraper {
      process '.regular del.priceOld', de => ['text', sub{/([.,\d]+)/;$1}];
      process '.sale span.price', por => ['text', sub{/([.,\d]+)/;$1}];
      process '.priceBankBill', avista => ['text', sub{/([.,\d]+)/;$1}];
      process '.priceBankBill', desconto => ['text', sub{/(\d+%)/;$1}];
      process '.parcel', parcelamento => 'text';
}}});

sub get_price {
  my ($self) = @_;
  return $self->_dom->scrape(URI->new($self->url));
}

1;

package main;
use DDP;
my $test = Crawler::Price::Americanas->new(
  url => 'http://www.americanas.com.br/produto/111259015/camera-digital-3d-sony-cyber-shot-dsc-wx100-18.2mp-c-10x-zoom-optico-cartao-8gb-preta'
)->get_price; 
p$test;
