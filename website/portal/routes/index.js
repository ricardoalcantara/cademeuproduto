var ES = require('es');

var config = require('../config.json');
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index');
};

exports.busca = function(req, res){
  var es = ES.createClient(config.es.config);

  var q = req.query.q;
  var page = req.query.page;

  if(q == "") {
    res.redirect('/');
    return;
  }

  if (page == undefined ||
      page == "") {
    page = 1
  }

  var filter = /[-+'"*@._.]/g;
  q = q.replace(filter, "");

  var query = {
    size: 20,
    from: ((page - 1) * 10),
    query: {
        filtered: {
            query: {
                query_string: {
                    query: q
                }
            },
            filter: {
                term: { available: true }
            }
        }
    },
    facets:{
      store:{
          terms:{
              field: "store"
            }
        }
    },
    suggest : {
      suggest_1 : {
        text : q,
        term : {
          field : "title"
        }
      }
    }
  };
  var model = {};
  es.search(config.es.index.product, query, function(err, data){
    if (err) console.log(err);

    model.result = data;
    model.q = q;
    model.page = page;
    res.render('busca', model);
  });
};

exports.store = function(req, res){
  var es = ES.createClient(config.es.config);

  var q = req.query.q;
  var page = req.query.page;
  var store = req.params.store;

  if(q == "") {
    res.redirect('/');
    return;
  }

  if (page == undefined ||
      page == "") {
    page = 1
  }

  var filter = /[-+'"*@._.]/g;
  q = q.replace(filter, "");
  
  var query = {
    size: 20,
    from: ((page - 1) * 10),
    query: {
        filtered: {
            query: {
                query_string: {
                    query: q
                }
            },
            filter: {
              and : [
                {
                  term: { 
                    available: true,
                  }
                },
                {
                  term: { 
                    store: store
                  }
                }
              ]
            }
        }
    },
    facets:{
      store:{
          terms:{
              field: "store"
            }
        }
    },
    suggest : {
      suggest_1 : {
        text : q,
        term : {
          field : "title"
        }
      }
    }
  };
  var model = {};
  es.search(config.es.index.product, query, function(err, data){
    if (err) console.log(err);
    
    model.result = data;
    model.q = q;
    model.page = page;
    res.render('busca', model);
  });
};
